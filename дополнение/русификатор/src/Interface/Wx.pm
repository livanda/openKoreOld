#########################################################################
#  OpenKore - WxWidgets Interface
#  You need:
#  * WxPerl (the Perl bindings for WxWidgets) - http://wxperl.sourceforge.net/
#
#  More information about WxWidgets here: http://www.wxwidgets.org/
#
#  Copyright (c) 2004,2005,2006,2007 OpenKore development team
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#
#########################################################################
#  modified:
#  ICQ266048166 Click Wx-Interface v15 for OpenKore 2.0.x and hight
#  To change only from sanction Click
#########################################################################
package Interface::Wx;

# Note: don't use wxTimer for anything important. It's known to cause reentrancy issues!

BEGIN {
	require Wx::Perl::Packager if ($^O eq 'MSWin32');
}

use strict;
use Wx ':everything';
use Wx::Event qw(EVT_CLOSE EVT_MENU EVT_MENU_OPEN EVT_LISTBOX_DCLICK
		EVT_CHOICE EVT_TIMER EVT_TASKBAR_LEFT_DOWN EVT_KEY_DOWN
		EVT_BUTTON);
use Time::HiRes qw(time sleep);
use File::Spec;
use FindBin qw($RealBin);


use Globals;
use Interface;
use base qw(Wx::App Interface);
use Modules;
use Field;
use Interface::Wx::Dock;
use Interface::Wx::MapViewer;
use Interface::Wx::LogView;
use Interface::Wx::Console;
use Interface::Wx::Input;
use Interface::Wx::ItemList;
use Interface::Wx::DockNotebook;
use Interface::Wx::PasswordDialog;
use Interface::Wx::Start;
#use Interface::Wx::Inventory;
use Interface::Wx::Emotion;
use Interface::Wx::Stats;
use AI;
use Settings qw(%sys);
use Plugins;
use Misc;
use Commands;
use Utils;


our $CVS;
our ($iterationTime, $updateUITime, $updateUITime2);


sub OnInit {
	my $self = shift;

	$CVS = ($Settings::CVS =~ /SVN/);
	$self->createInterface;
	$self->iterate;

	my $onChat = sub { $self->onChatAdd(@_); };
	$self->{hooks} = Plugins::addHooks(
		['loadfiles',               sub { $self->onLoadFiles(@_); }],
		['postloadfiles',           sub { $self->onLoadFiles(@_); }],
		['parseMsg/addPrivMsgUser', sub { $self->onAddPrivMsgUser(@_); }],
		['initialized',             sub { $self->onInitialized(@_); }],
		['ChatQueue::add',          $onChat],
		['packet_selfChat',         $onChat],
		['packet_privMsg',          $onChat],
		['packet_sentPM',           $onChat],
		['mainLoop_pre',            sub { $self->onUpdateUI(); }]
	);

	$self->{history} = [];
	$self->{historyIndex} = -1;

	$self->{frame}->Update;

	return 1;
}

sub DESTROY {
	my $self = shift;
	Plugins::delHooks($self->{hooks});
}


######################
## METHODS
######################

sub mainLoop {
	my ($self) = @_;
	my $timer = new Wx::Timer($self, 246);
	# Start the real main loop in 100 msec, so that the UI has
	# the chance to layout correctly.
	EVT_TIMER($self, 246, sub { $self->realMainLoop(); });
	$timer->Start(100, 1);
	$self->MainLoop;
}

sub realMainLoop {
	my ($self) = @_;
	my $timer = new Wx::Timer($self, 247);
	my $sleepTime = $config{sleepTime};
	my $quitting;
	my $sub = sub {
		return if ($quitting);
		if ($quit) {
			$quitting = 1;
			$self->ExitMainLoop;
			$timer->Stop;
			return;
		} elsif ($self->{iterating}) {
			return;
		}

		$self->{iterating}++;

		if ($sleepTime ne $config{sleepTime}) {
			$sleepTime = $config{sleepTime};
			$timer->Start(($sleepTime / 1000) > 0
				? ($sleepTime / 1000)
				: 10);
		}
		main::mainLoop();

		$self->{iterating}--;
	};

	EVT_TIMER($self, 247, $sub);
	$timer->Start(($sleepTime / 1000) > 0
		? ($sleepTime / 1000)
		: 10);
}

sub iterate {
	my $self = shift;

	if ($self->{iterating} == 0) {
		$self->{console}->Refresh;
		$self->{console}->Update;
	}
	$self->Yield();
	$iterationTime = time;
}

sub getInput {
	my $self = shift;
	my $timeout = shift;
	my $msg;

	if ($timeout < 0) {
		while (!defined $self->{input} && !$quit) {
			$self->iterate;
			sleep 0.01;
		}
		$msg = $self->{input};

	} elsif ($timeout == 0) {
		$msg = $self->{input};

	} else {
		my $begin = time;
		until (defined $self->{input} || time - $begin > $timeout || $quit) {
			$self->iterate;
			sleep 0.01;
		}
		$msg = $self->{input};
	}

	undef $self->{input};
	undef $msg if (defined($msg) && $msg eq "");

	# Make sure we update the GUI. This is to work around the effect
	# of functions that block for a while
	$self->iterate if (timeOut($iterationTime, 0.05));

	return $msg;
}

sub query {
	my $self = shift;
	my $message = shift;
	my %args = @_;

	$args{title} = "Query" if (!defined $args{title});
	$args{cancelable} = 1 if (!exists $args{cancelable});

	$message = wrapText($message, 70);
	$message =~ s/\n$//s;
	my $dialog;
	if ($args{isPassword}) {
		# WxPerl doesn't support wxPasswordEntryDialog :(
		$dialog = new Interface::Wx::PasswordDialog($self->{frame}, $message, $args{title});
	} else {
		$dialog = new Wx::TextEntryDialog($self->{frame}, $message, $args{title});
	}
	while (1) {
		my $result;
		if ($dialog->ShowModal == wxID_OK) {
			$result = $dialog->GetValue;
		}
		if (!defined($result) || $result eq '') {
			if ($args{cancelable}) {
				$dialog->Destroy;
				return undef;
			}
		} else {
			$dialog->Destroy;
			return $result;
		}
	}
}

sub showMenu {
	my $self = shift;
	my $message = shift;
	my $choices = shift;
	my %args = @_;

	$args{title} = "Menu" if (!defined $args{title});
	$args{cancelable} = 1 if (!exists $args{cancelable});

	$message = wrapText($message, 70);
	$message =~ s/\n$//s;
	my $dialog = new Wx::SingleChoiceDialog($self->{frame},
		$message, $args{title}, $choices);
	while (1) {
		my $result;
		if ($dialog->ShowModal == wxID_OK) {
			$result = $dialog->GetSelection;
		}
		if (!defined($result)) {
			if ($args{cancelable}) {
				$dialog->Destroy;
				return -1;
			}
		} else {
			$dialog->Destroy;
			return $result;
		}
	}
}

sub writeOutput {
	my $self = shift;
	$self->{console}->add(@_);
	# Make sure we update the GUI. This is to work around the effect
	# of functions that block for a while
	$self->iterate if (timeOut($iterationTime, 0.5));
}

sub title {
	my $self = shift;
	my $title = shift;



	my $charName = $chars[$config{'char'}]{'name'};
	$charName .= ': ' if defined $charName;
	if ($net->getState() == Network::IN_GAME) {
		my ($basePercent, $jobPercent, $zeny, $weight, $pos);

		$basePercent = sprintf("%.2f", $chars[$config{'char'}]{'exp'} / $chars[$config{'char'}]{'exp_max'} * 100) if $chars[$config{'char'}]{'exp_max'};
		$jobPercent = sprintf("%.2f", $chars[$config{'char'}]{'exp_job'} / $chars[$config{'char'}]{'exp_job_max'} * 100) if $chars[$config{'char'}]{'exp_job_max'};
		$zeny = formatNumber($char->{'zenny'}) if (defined($char->{'zenny'}));
		$weight = sprintf("%.1f", $char->{'weight'}/$char->{'weight_max'} * 100). "%" if $char->{'weight_max'};
		$pos = "$char->{pos_to}{x},$char->{pos_to}{y} $field{'name'}" if ($char->{pos_to} && $field{'name'});

		# Translation Comment: Interface Title with character status
		$title = "${charName}B$chars[$config{'char'}]{'lv'}($basePercent%) J$chars[$config{'char'}]{'lv_job'}($jobPercent%) z$zeny w$weight: ${pos} - Click WX - promo free";
	}
	if (defined $title) {
		if ($title ne $self->{title}) {
			$self->{frame}->SetTitle("$title");
			$self->{title} = "$title";
		}
	} else {
		return $self->{title};
	}
}

sub displayUsage {
	my $self = shift;
	my $text = shift;
	print $text;
}

sub errorDialog {
	my $self = shift;
	my $msg = shift;
	my $fatal = shift;

	my $title = ($fatal) ? "Fatal error" : "Error";
	$self->{iterating}++;
	Wx::MessageBox($msg, "$title - $Settings::NAME", wxICON_ERROR, $self->{frame});
	$self->{iterating}--;
}

sub beep {
	Wx::Bell();
}


#########################
## INTERFACE CREATION
#########################


sub createInterface {
	my $self = shift;

	### Main window
	my $frame = $self->{frame} = new Wx::Frame(undef, -1, $Settings::NAME);
	$self->{title} = $frame->GetTitle();


	### Menu bar
	$self->createMenuBar;

	### Vertical box sizer
	my $vsizer = $self->{vsizer} = new Wx::BoxSizer(wxVERTICAL);
	$frame->SetSizer($vsizer);

	### Horizontal panel with HP/SP/Exp box
	$self->createInfoPanel;
	$self->createInfoPanel2;
	$self->createInfoPanel5;
	$self->createInfoPanel3;

	## Splitter with console and another splitter
	my $splitter = new Wx::SplitterWindow($frame, 928, wxDefaultPosition, wxDefaultSize,
		wxSP_LIVE_UPDATE);
	$self->{splitter} = $splitter;
	$vsizer->Add($splitter, 1, wxGROW);
#	$splitter->SetMinimumPaneSize(50);
	$self->createSplitterContent;


	### Input field
	$self->createInputField;

	$self->createInfoPanel7;

	### Status bar
	my $statusbar = $self->{statusbar} = new Wx::StatusBar($frame, -1, wxST_SIZEGRIP);
	$statusbar->SetFieldsCount(4);
	$statusbar->SetStatusWidths(-1, -1, 65, 250);
	$frame->SetStatusBar($statusbar);


	#################

	$frame->SetSizeHints(300, 250);
	$frame->SetClientSize(730, 400);
	$frame->SetIcon(Wx::GetWxPerlIcon);
	$frame->Show(1);
	$self->{infoPanel7}->Show(0);
	EVT_CLOSE($frame, \&onClose);

	# For some reason the input box doesn't get focus even if
	# I call SetFocus(), so do it in 100 msec.
	# And the splitter window's sash position is placed incorrectly
	# if I call SetSashGravity immediately.
	my $timer = new Wx::Timer($self, 73289);
	EVT_TIMER($self, 73289, sub {
		$self->{inputBox}->SetFocus;
		$self->{notebook}->switchPage('Console');
#		$splitter->SetSashGravity(1);
	});
	$timer->Start(500, 1);

	# Hide console on Win32
	if ($^O eq 'MSWin32' && $sys{wxHideConsole}) {
		eval 'use Win32::Console; Win32::Console->new(STD_OUTPUT_HANDLE)->Free();';
	}
}


sub createMenuBar {
	my $self = shift;
	my $menu = $self->{menu} = new Wx::MenuBar;
	my $frame = $self->{frame};
	$frame->SetMenuBar($menu);
	EVT_MENU_OPEN($self->{frame}, sub { $self->onMenuOpen; });

	# Program menu
	my $opMenu = new Wx::Menu;
	$self->{mPause}  = $self->addMenu($opMenu, '&�����', \&onDisableAI, '������������� ���������� ����');
	$self->{mManual} = $self->addMenu($opMenu, '&� ������', \&onManualAI, '����� ���� ����, ��������� ������� ������');
	$self->{mResume} = $self->addMenu($opMenu, '&�������� ����!', \&onEnableAI, '������������ �������������� �����������');
	$opMenu->AppendSeparator;
	$self->addMenu($opMenu, '����������� 100 ��������� �������', \&onCopyLastOutput);
		$self->addMenu($opMenu, '� &����', \&onMinimizeToTray, '�������� � ����');
		$opMenu->AppendSeparator;
	$self->addMenu($opMenu, '�&����	Ctrl-W', \&quit, '����� �� ����');
	$menu->Append($opMenu, '�&����');

	# Info menu
	my $infoMenu = new Wx::Menu;
	$self->addMenu($infoMenu, '&������	Alt-S',	sub { Commands::run("s"); });
	$self->addMenu($infoMenu, '�&����',	sub { Commands::run("st"); });
	$self->addMenu($infoMenu, '�&����',		sub { Commands::run("skills"); });
	$self->addMenu($infoMenu, '&����� � ���������',	sub { Commands::run("card list"); });
	$self->addMenu($infoMenu, '&���������	Alt-I',	sub { Commands::run("i"); });
	$self->addMenu($infoMenu, '&��������� �����',	sub { Commands::run("storage"); });
	$self->addMenu($infoMenu, '&��������� ������',	sub { Commands::run("cart"); });
	$infoMenu->AppendSeparator;
	$self->addMenu($infoMenu, '&������	Alt-P',	sub { Commands::run("pl"); });
	$self->addMenu($infoMenu, '&����	Alt-M',	sub { Commands::run("ml"); });
	$self->addMenu($infoMenu, '&����',		sub { Commands::run("petl"); });
	$self->addMenu($infoMenu, '&�������',		sub { Commands::run("vl"); });
	$self->addMenu($infoMenu, '&NPCs',		sub { Commands::run("nl"); });
	$self->addMenu($infoMenu, '&�������',		sub { Commands::run("portals"); });
	$infoMenu->AppendSeparator;
	$self->addMenu($infoMenu, '&������ ������',	sub { Commands::run("friend"); });
	$self->addMenu($infoMenu, '&������ �������',	sub { Commands::run("guild member"); });
	$infoMenu->AppendSeparator;
	$self->addMenu($infoMenu, '&�������� ������',	sub { Commands::run("damage"); });
	$infoMenu->AppendSeparator;
	$self->addMenu($infoMenu, '�&���� ������',	sub { Commands::run("exp reset"); });
	$infoMenu->AppendSeparator;
	$self->addMenu($infoMenu, '&����� �� Exp	Alt+E',	sub { Commands::run("exp"); });
	$self->addMenu($infoMenu, '��&��� �� �����',	sub { Commands::run("exp monster"); });
	$self->addMenu($infoMenu, '���&�� �� ������',	sub { Commands::run("exp item"); });
	$self->addMenu($infoMenu, '�&����� �����',	sub { Commands::run("exp report"); });
	$menu->Append($infoMenu, '&����');

	# View menu
	my $viewMenu = $self->{viewMenu} = new Wx::Menu;
	$self->addMenu($viewMenu,
		'&�����	Ctrl-M',	\&onMapToggle, '�����');
	$self->{infoBarToggle} = $self->addCheckMenu($viewMenu,
		'&������ ����',		\&onInfoBarToggle, '��������, ������ �������������� ������');
	$self->{chatLogToggle} = $self->addCheckMenu($viewMenu,
		'&��� ����',		\&onChatLogToggle, '��������, ������ ��� ����');
	$self->{InvLogToggle} = $self->addCheckMenu($viewMenu,
#		'&Inventory',		\&onInvLogToggle, '��������, ������ ��� ���������');
		'&Inventory',		\&onInitialized, '��������, ������ ��� ���������'); 
	$viewMenu->AppendSeparator;
	$self->addMenu($viewMenu,
		'&�����...',		\&onFontChange, '�������� ����� �������');
	$viewMenu->AppendSeparator;
	$self->addMenu($viewMenu,
		'&�������� �������',	\&onClearConsole);
	$menu->Append($viewMenu, '&���');

	# Settings menu
	my $settingsMenu = new Wx::Menu;
	$self->createSettingsMenu($settingsMenu) if ($self->can('createSettingsMenu'));
	$self->addMenu($settingsMenu, '&������...', \&onAdvancedConfig, '�������� �������');
	$self->addMenu($settingsMenu, '&pickupitems...', \&onpickup, '�������� pickupitems');
	$self->addMenu($settingsMenu, '&�����������',	sub { Commands::run("reload all"); });
	$self->addMenu($settingsMenu, '�&�������������',	sub { Commands::run("relog"); });
	$settingsMenu->AppendSeparator;
	$self->addMenu($settingsMenu, '&���',		sub { Commands::run('chat create "AFK" [<20> <0> <0>]'); });
	$self->addMenu($settingsMenu, '&������� ���',		sub { Commands::run('chat leave'); });
	$settingsMenu->AppendSeparator;
	$self->addMenu($settingsMenu, '&���� ����� �� ����',	sub { Commands::run("as"); });
	$settingsMenu->AppendSeparator;
	$self->addMenu($settingsMenu, '&��������',	sub { Commands::run("tele"); });
	$self->addMenu($settingsMenu, '&Butterfly Wing',	sub { Commands::run("is Butterfly Wing"); });
	$self->addMenu($settingsMenu, '&���������',	sub { Commands::run("respawn"); });
	$settingsMenu->AppendSeparator;
	$self->addMenu($settingsMenu, '&Butterfly Wing->Storage->Sell->Buy',	\&onBw_St_Sl_By, 'Butterfly Wing->Storage->Sell->Buy');
	$settingsMenu->AppendSeparator;
	$self->addMenu($settingsMenu, '&autostorage',	sub { Commands::run("autostorage"); });
	$self->addMenu($settingsMenu, '&autobuy',	sub { Commands::run("autobuy"); });
	$self->addMenu($settingsMenu, '&autosell',	sub { Commands::run("autosell"); });
	$menu->Append($settingsMenu, '&�������');
	$self->createSettingsMenu2($settingsMenu) if ($self->can('createSettingsMenu2'));

	# Char menu
	my $charMenu = new Wx::Menu();
	$self->addMenu($charMenu, '&��� �0', \&onChar0, '����� ����');
	$self->addMenu($charMenu, '&��� �1', \&onChar1, '����� ����');
	$self->addMenu($charMenu, '&��� �2', \&onChar2, '����� ����');
	$self->addMenu($charMenu, '&��� �3', \&onChar3, '����� ����');
	$menu->Append($charMenu, '&����� ����');

	# Individual menu
	my $individualMenu = new Wx::Menu();
	$self->addMenu($individualMenu, '&��������',	sub { Commands::run("ss 135"); });
	$self->addMenu($individualMenu, '&����',	sub { Commands::run("ss 51"); });
	$self->addMenu($individualMenu, '&Chase Walk',	sub { Commands::run("ss 389"); });
	$individualMenu->AppendSeparator;
	$self->addMenu($individualMenu, '&����� ������',	sub { Commands::run("eq Arrow"); });
	$self->addMenu($individualMenu, '&����� �����.������',	sub { Commands::run("eq Silver Arrow"); });
	$menu->Append($individualMenu, '&����������');

	# Party menu
	my $partyMenu = new Wx::Menu();
	$self->{iParty} = $self->addMenu($partyMenu, '&����� ����',		sub { Commands::run("conf partyAuto 0"); });
	$self->{dParty} = $self->addMenu($partyMenu, '&��������� ����',		sub { Commands::run("conf partyAuto 1"); });
	$self->{aParty} = $self->addMenu($partyMenu, '&��������� ����',		sub { Commands::run("conf partyAuto 2"); });
	$partyMenu->AppendSeparator;
	$self->{shareParty1}  = $self->addMenu($partyMenu, '&������ ����',		sub { Commands::run("party share 1"); });
	$self->{shareParty0}  = $self->addMenu($partyMenu, '&�� ������ ����',		sub { Commands::run("party share 0"); });
	$partyMenu->AppendSeparator;
	$self->addMenu($partyMenu, '�&������� � ����',	sub { Commands::run("party join 1"); });
	$self->addMenu($partyMenu, '&���������� �� ����',	sub { Commands::run("party join 0"); });
	$partyMenu->AppendSeparator;
	$self->addMenu($partyMenu, '&����� �� ����',	sub { Commands::run("party leave"); });
	$self->addMenu($partyMenu, '&��� � ����',	sub { Commands::run("party"); });
	$self->createPartyMenu($partyMenu) if ($self->can('createPartyMenu'));
	$menu->Append($partyMenu, '&������');

	# Deal menu
	my $dealMenu = new Wx::Menu();
	$self->{iDeal} = $self->addMenu($dealMenu, '&����� ������',		sub { Commands::run("conf dealAuto 0"); });
	$self->{dDeal} = $self->addMenu($dealMenu, '&��������� ������',		sub { Commands::run("conf dealAuto 1"); });
	$self->{aDeal} = $self->addMenu($dealMenu, '&��������� ������',		sub { Commands::run("conf dealAuto 2"); });
	$dealMenu->AppendSeparator;
	$self->addMenu($dealMenu, '�&�����',		sub { Commands::run("deal"); });
	$self->addMenu($dealMenu, '�&������� ������',		sub { Commands::run("deal no"); });
	$self->addMenu($dealMenu, '�&�� � ������',		sub { Commands::run("dl"); });
	$self->createDealMenu($dealMenu) if ($self->can('createDealMenu'));
	$menu->Append($dealMenu, '&������');

	# routevstele menu
	my $routevsteleMenu = new Wx::Menu();
	$self->addMenu($routevsteleMenu, '&Move+Route 0',		\&onRouteStop, 'Move+Route 0');
	$routevsteleMenu->AppendSeparator;
	$self->addMenu($routevsteleMenu, '&TeleSearch 1',		sub { Commands::run("conf teleportAuto_search 1"); });
	$self->addMenu($routevsteleMenu, '&RouteWalk 0',		sub { Commands::run("conf route_randomWalk 0"); });
	$routevsteleMenu->AppendSeparator;
	$self->addMenu($routevsteleMenu, '&TeleSearch 0',		sub { Commands::run("conf teleportAuto_search 0"); });
	$self->addMenu($routevsteleMenu, '&RouteWalk 1',		sub { Commands::run("conf route_randomWalk 1"); });
	$self->createroutevsteleMenu($routevsteleMenu) if ($self->can('createroutevsteleMenu'));
	$menu->Append($routevsteleMenu, '&Route vs Tele');
 

	# Help menu
	my $helpMenu = new Wx::Menu();
	$self->addMenu($helpMenu, '&������� �� ����� ������ �� ���� �����',		\&onWMZ, '����� ������� ������� Click');
	$self->addMenu($helpMenu, '&������� ������ http://manual.rofan.ru/	F1',		\&onManual, '������ �����, ����� ����������)))');
	$self->addMenu($helpMenu, '&���������� ������ openkore.sourceforge.net',		\&onManual2, '������ �����, ����� ����������)))');
	$self->addMenu($helpMenu, '&����� � ����� �� rofan.ru	Shift-F1',	\&onForum, '�������� ����� http://rofan.ru/index.php');
	$self->addMenu($helpMenu, '&����������� � ���� Wx',	\&onTopic, '� ����� ��� Wx ���������');
	$self->addMenu($helpMenu, '&Help Click',		\&onEgold, 'Help Click');
	$self->addMenu($helpMenu, '&ICQ Click',		\&onICQ, 'ICQ Click');
	$self->createHelpMenu($helpMenu) if ($self->can('createHelpMenu'));
	$menu->Append($helpMenu, '&������');
}

sub createInfoPanel {
	my $self = shift;
	my $frame = $self->{frame};
	my $vsizer = $self->{vsizer};
	my $infoPanel = $self->{infoPanel} = new Wx::Panel($frame, -1);

	my $hsizer = new Wx::BoxSizer(wxHORIZONTAL);
	my $label = new Wx::StaticText($infoPanel, -1, "HP: ");
	$hsizer->Add($label, 0, wxLEFT, 3);


	## HP
	my $hpBar = $self->{hpBar} = new Wx::Gauge($infoPanel, -1, 100,
		wxDefaultPosition, [0, $label->GetBestSize->GetHeight + 2],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
	$hsizer->Add($hpBar, 1, wxRIGHT, 8);

	$label = new Wx::StaticText($infoPanel, -1, "SP: ");
	$hsizer->Add($label, 0);

	## SP
	my $spBar = $self->{spBar} = new Wx::Gauge($infoPanel, -1, 100,
		wxDefaultPosition, [0, $label->GetBestSize->GetHeight + 2],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
	$hsizer->Add($spBar, 1, wxRIGHT, 8);

	$label = new Wx::StaticText($infoPanel, -1, "Exp: ");
	$hsizer->Add($label, 0);

	## Exp and job exp
	my $expBar = $self->{expBar} = new Wx::Gauge($infoPanel, -1, 100,
		wxDefaultPosition, [0, $label->GetBestSize->GetHeight + 2],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
	$hsizer->Add($expBar, 1);
	my $jobExpBar = $self->{jobExpBar} = new Wx::Gauge($infoPanel, -1, 100,
		wxDefaultPosition, [0, $label->GetBestSize->GetHeight + 2],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
	$hsizer->Add($jobExpBar, 1, wxRIGHT, 8);

	$label = new Wx::StaticText($infoPanel, -1, "���: ");
	$hsizer->Add($label, 0);

	## Weight
	my $weightBar = $self->{weightBar} = new Wx::StaticText($infoPanel, -1, "                        ");
	$hsizer->Add($weightBar, 2);


	$label = new Wx::Button($infoPanel, 40, "&�����", wxDefaultPosition, [50, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($self, 40, \&onStats);

	$label = new Wx::Button($infoPanel, 41, "&������", wxDefaultPosition, [50, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($self, 41, \&onEmotions);

	$label = new Wx::Button($infoPanel, 47, "&�����", wxDefaultPosition, [50, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($self, 47, \&onMapToggle);


	$infoPanel->SetSizerAndFit($hsizer);
	$vsizer->Add($infoPanel, 0, wxGROW);
}

sub createInfoPanel2 {
	my $self = shift;
	my $frame = $self->{frame};
	my $vsizer = $self->{vsizer};
	my $infoPanel2 = $self->{infoPanel2} = new Wx::Panel($frame, -1);
	my $hsizer = new Wx::BoxSizer(wxHORIZONTAL);
	my $label = new Wx::StaticText($infoPanel2, -1, "HP:");
	$hsizer->Add($label, 0, wxLEFT, 3);

	## HP
	my $hpbar0 = $self->{hpBar0} = new Wx::StaticText($infoPanel2, -1, "                                            ");
	$hsizer->Add($hpbar0, 1, wxRIGHT, 8);

	$label = new Wx::StaticText($infoPanel2, -1, "SP:");
	$hsizer->Add($label, 0);
	## SP
	my $spBar0 = $self->{spBar0} = new Wx::StaticText($infoPanel2, -1, "                                            ");
	$hsizer->Add($spBar0, 1, wxRIGHT, 8);


	$label = new Wx::StaticText($infoPanel2, -1, "Zeny:");
	$hsizer->Add($label, 0);
	##Zeny
	my $zenys = $self->{zenys} = new Wx::StaticText($infoPanel2, -1, "                             ");
	$hsizer->Add($zenys, 1);
	

	$label = new Wx::StaticText($infoPanel2, -1, "|");
	$hsizer->Add($label, 0, wxRIGHT, 3);

#	$label->SetBackgroundColour (new Wx::Colour(0, 0, 0));

	$label = $self->{SitStand} = new Wx::Button($infoPanel2, 47, "&�����", wxDefaultPosition, [45, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
			if ($char->{sitting} eq 1){ Commands::run("stand");
			} else { Commands::run("sit");}
		});

	$label = new Wx::StaticText($infoPanel2, -1, "|");
	$hsizer->Add($label, 0, wxRIGHT, 3);

	$label = new Wx::Button($infoPanel2, 47, "&��� ���", wxDefaultPosition, [45, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, \&onManualAI);

	$label = $self->{AutoPause} = new Wx::Button($infoPanel2, 47, "&�����", wxDefaultPosition, [45, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, \&onEnableAI);

	$label = new Wx::StaticText($infoPanel2, -1, "|");
	$hsizer->Add($label, 0, wxRIGHT, 3);

	$label = new Wx::Button($infoPanel2, 47, "&����", wxDefaultPosition, [50, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47,	sub { Commands::run("tele"); });

	$label = new Wx::Button($infoPanel2, 47, "&RelConf", wxDefaultPosition, [50, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47,	sub { Commands::run("reload conf"); });

	$label = new Wx::Button($infoPanel2, 47, "&Domains", wxDefaultPosition, [45, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47,	sub {
					if ($config{squelchDomains} eq "0") { Commands::run("conf squelchDomains ai_attack, attacked, attackedMiss, attackMon, attackMonMiss, looter");
					} else { Commands::run("conf squelchDomains 0")}
		});

	$infoPanel2->SetSizerAndFit($hsizer);
	$vsizer->Add($infoPanel2, 0, wxGROW);
}

sub createInfoPanel3 {
	my $self = shift;
	my $frame = $self->{frame};
	my $vsizer = $self->{vsizer};
	my $infoPanel3 = $self->{infoPanel3} = new Wx::Panel($frame, -1);

	my $hsizer = new Wx::BoxSizer(wxHORIZONTAL);

	my $label = new Wx::StaticText($infoPanel3, -1, " ������: ");
	$hsizer->Add($label, 0);

	my $stat = $self->{stat} = new Wx::StaticText($infoPanel3, -1, "");
	$hsizer->Add($stat, 1, wxRIGHT, 10);
	$infoPanel3->SetSizerAndFit($hsizer);
	$vsizer->Add($infoPanel3, 0, wxGROW);
}

sub createInfoPanel5 {
	my $self = shift;
	my $frame = $self->{frame};
	my $vsizer = $self->{vsizer};
	my $infoPanel5 = $self->{infoPanel5} = new Wx::Panel($frame, -1);
	my $hsizer = new Wx::BoxSizer(wxHORIZONTAL);
	my $label = new Wx::StaticText($infoPanel5, -1, "����� �� lvl: ");
	$hsizer->Add($label, 0, wxLEFT, 3);

	my $lvl = $self->{lvl} = new Wx::StaticText($infoPanel5, -1, "");
	$hsizer->Add($lvl, 1, wxRIGHT, 8);


	$label = new Wx::StaticText($infoPanel5, -1, " ����� �� job: ");
	$hsizer->Add($label, 0);

	my $job = $self->{job} = new Wx::StaticText($infoPanel5, -1, "");
	$hsizer->Add($job, 1, wxRIGHT, 8);

	$label = new Wx::StaticText($infoPanel5, -1, " ����� �� ���: ");
	$hsizer->Add($label, 0);

	my $zen = $self->{zen} = new Wx::StaticText($infoPanel5, -1, "");
	$hsizer->Add($zen, 1, wxRIGHT, 8);

	$infoPanel5->SetSizerAndFit($hsizer);
	$vsizer->Add($infoPanel5, 0, wxGROW);
}

sub createInfoPanel7 {
	my $self = shift;
	my $frame = $self->{frame};
	my $vsizer = $self->{vsizer};
	my $infoPanel7 = $self->{infoPanel7} = new Wx::Panel($frame, -1);
	my $hsizer = new Wx::BoxSizer(wxHORIZONTAL);

	my $label = new Wx::Button($infoPanel7, 47, "&talk resp", wxDefaultPosition, [50, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp");
		});
#	$hsizer->Add($label, 0, wxLEFT, 3);

	$label = new Wx::Button($infoPanel7, 47, "&0", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 0");
		});

	$label = new Wx::Button($infoPanel7, 47, "&1", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 1");
		});

	$label = new Wx::Button($infoPanel7, 47, "&2", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 2");
		});

	$label = new Wx::Button($infoPanel7, 47, "&3", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 3");
		});

	$label = new Wx::Button($infoPanel7, 47, "&4", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 4");
		});

	$label = new Wx::Button($infoPanel7, 47, "&5", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 5");
		});

	$label = new Wx::Button($infoPanel7, 47, "&6", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 6");
		});

	$label = new Wx::Button($infoPanel7, 47, "&7", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 7");
		});

	$label = new Wx::Button($infoPanel7, 47, "&8", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 8");
		});

	$label = new Wx::Button($infoPanel7, 47, "&9", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk resp 9");
		});

	$label = new Wx::StaticText($infoPanel7, -1, "  talk num ");
	$hsizer->Add($label, 0, wxRIGHT, 3);

	my $inputTalk = $self->{inputTalk} = new Interface::Wx::Input($infoPanel7);
	$inputTalk->onEnter($self, \&onTalkEnter);
	$hsizer->Add($inputTalk, 1, wxGROW);

	$label = new Wx::Button($infoPanel7, 47, "&talk cont", wxDefaultPosition, [50, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("talk cont");
		});;

	$label = new Wx::StaticText($infoPanel7, -1, "      ");
	$hsizer->Add($label, 0, wxRIGHT, 3);

	$label = new Wx::Button($infoPanel7, 47, "&store", wxDefaultPosition, [35, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("store");
		});;

	$label = new Wx::StaticText($infoPanel7, -1, " |");
	$hsizer->Add($label, 0, wxRIGHT, 3);

	$label = new Wx::Button($infoPanel7, 47, "&sell", wxDefaultPosition, [35, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("i");
		Commands::run("sell");
		});;
	$label = new Wx::Button($infoPanel7, 47, "&list", wxDefaultPosition, [35, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("sell list");
		});;
	$label = new Wx::Button($infoPanel7, 47, "&cancel", wxDefaultPosition, [35, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("sell cancel");
		});;
	$label = new Wx::Button($infoPanel7, 47, "&done", wxDefaultPosition, [35, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47, sub {
		Commands::run("sell done");
		});;

	$label = new Wx::StaticText($infoPanel7, -1, "                                    ");
	$hsizer->Add($label, 0, wxRIGHT, 3);

	$label = new Wx::Button($infoPanel7, 47, "&�", wxDefaultPosition, [15, 20],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$hsizer->Add($label, 0);
		EVT_BUTTON($label, 47,	sub { $self->{infoPanel7}->Show(0); $self->{frame}->Layout;});

	$infoPanel7->SetSizerAndFit($hsizer);
	$vsizer->Add($infoPanel7, 0, wxGROW);
}

sub createInputField {
	my $self = shift;
	my $vsizer = $self->{vsizer};
	my $frame = $self->{frame};

	my $hsizer = new Wx::BoxSizer(wxHORIZONTAL);
	$vsizer->Add($hsizer, 0, wxGROW);

	my $targetBox = $self->{targetBox} = new Wx::ComboBox($frame, -1, "", wxDefaultPosition,
		[115, 0]);
	$targetBox->SetName('targetBox');
	$hsizer->Add($targetBox, 0, wxGROW);
	EVT_KEY_DOWN($self, \&onTargetBoxKeyDown);

	my $inputBox = $self->{inputBox} = new Interface::Wx::Input($frame);
	$inputBox->onEnter($self, \&onInputEnter);
	$hsizer->Add($inputBox, 1, wxGROW);

	my $choice = $self->{inputType} = new Wx::Choice($frame, 456, wxDefaultPosition, wxDefaultSize,
			['�������', '����� ���', '���� ���', '����� ���']);
	$choice->SetSelection(0);
	EVT_CHOICE($self, 456, sub { $inputBox->SetFocus; });
	$hsizer->Add($choice, 0, wxGROW);
}

sub createSplitterContent {
	my $self = shift;
	my $splitter = $self->{splitter};
	my $frame = $self->{frame};

	## Dockable notebook with console and chat log
	my $notebook = $self->{notebook} = new Interface::Wx::DockNotebook($splitter, -1);
	$notebook->SetName('notebook');
	my $page = $notebook->newPage(0, '�������');
	my $console = $self->{console} = new Interface::Wx::Console($page);
	$page->set($console);

	$page = $notebook->newPage(1, 'Chat Log', 0);
	my $chatLog = $self->{chatLog} = new Interface::Wx::LogView($page);
	$page->set($chatLog);
	$chatLog->addColor("selfchat", 0, 148, 0);
	$chatLog->addColor("pm", 142, 120, 0);
	$chatLog->addColor("p", 164, 0, 143);
	$chatLog->addColor("g", 0, 177, 108);
	$chatLog->addColor("warning", 214, 93, 0);


	## Parallel to the notebook is another sub-splitter
	my $subSplitter = new Wx::SplitterWindow($splitter, 583,
		wxDefaultPosition, wxDefaultSize, wxSP_LIVE_UPDATE);

	## Inside this splitter is a player/monster/item list, and a dock with map viewer

	my $itemList = $self->{itemList} = new Interface::Wx::ItemList($subSplitter);
	$itemList->onActivate(\&onItemListActivate, $self);
	$itemList->onRightClick(\&onItemListRightClick, $self);
	$self->customizeItemList($itemList) if ($self->can('customizeItemList'));
	$subSplitter->Initialize($itemList);


	# Dock
	my $mapDock = $self->{mapDock} = new Interface::Wx::Dock($subSplitter, -1, 'Map');
	$mapDock->Show(0);
	$mapDock->setHideFunc($self, sub {
		$subSplitter->Unsplit($mapDock);
		$mapDock->Show(0);
		$self->{inputBox}->SetFocus;
	});
	$mapDock->setShowFunc($self, sub {
		$subSplitter->SplitVertically($itemList, $mapDock, -$mapDock->GetBestSize->GetWidth);
		$mapDock->Show(1);
		$self->{inputBox}->SetFocus;
	});

	# Map viewer
	my $mapView = $self->{mapViewer} = new Interface::Wx::MapViewer($mapDock);
	$mapDock->setParentFrame($frame);
	$mapDock->set($mapView);
	$mapView->onMouseMove($self, \&onMapMouseMove);
	$mapView->onClick->add($self, \&onMapClick);
	$mapView->onMapChange($self, \&onMap_MapChange, $mapDock);
	$mapView->parsePortals("$Settings::tables_folder/portals.txt");
	if ($field && $char) {
		$mapView->set($field->name(), $char->{pos_to}{x}, $char->{pos_to}{y}, $field);
	}

	my $position;
	if (Wx::wxMSW()) {
		$position = 600;
	} else {
		$position = 545;
	}
	$splitter->SplitVertically($notebook, $subSplitter, $position);
}


sub addMenu {
	my ($self, $menu, $label, $callback, $help) = @_;

	$self->{menuIDs}++;
	my $item = new Wx::MenuItem(undef, $self->{menuIDs}, $label, $help);
	$menu->Append($item);
	EVT_MENU($self->{frame}, $self->{menuIDs}, sub { $callback->($self); });
	return $item;
}

sub addCheckMenu {
	my ($self, $menu, $label, $callback, $help) = @_;

	$self->{menuIDs}++;
	my $item = new Wx::MenuItem(undef, $self->{menuIDs}, $label, $help, wxITEM_CHECK);
	$menu->Append($item);
	EVT_MENU($self->{frame}, $self->{menuIDs}, sub { $callback->($self); }) if ($callback);
	return $item;
}


##########################
## INTERFACE UPDATING
##########################


sub onUpdateUI {
	my $self = shift;

	if (timeOut($updateUITime, 2)) {
		$self->updateStatusBar;
		$updateUITime = time;
	}

	if (timeOut($updateUITime2, 0.4)) {
		$self->updateItemList;
		$self->updateMapViewer;
		$updateUITime2 = time;
	}
}

sub updateStatusBar {
		my $total;
		my $mobi;
		my $y;
		my $z = $total;
		for (my $iii = 0; $iii < @monsters_Killed; $iii++) {
			next if ($monsters_Killed[$iii] eq "");
			$total += $monsters_Killed[$iii]{count};
		my $x = int(rand($iii+1));
		if ($total > $z) {$y = $x};
		$z = $total;
			$mobi = "$monsters_Killed[$y]{name} x $monsters_Killed[$y]{count}";
		}

##################

	my $self = shift;

	my ($statText, $clickText, $xyText, $aiText) = ('', '', '','');

	if ($self->{loadingFiles}) {
		$statText = sprintf("Loading files... %.0f%%", $self->{loadingFiles}{percent} * 100);
	} elsif (!$conState) {
		$statText = "Initializing...";
	} elsif ($conState == 1) {
		$statText = "Not connected";
	} elsif ($conState > 1 && $conState < 5) {
		$statText = "Connecting...";
	} elsif ($self->{mouseMapText}) {
		$statText = $self->{mouseMapText};
	}

	if ($char) {	$clickText = "death:$char->{'deathCount'} kills:$total $mobi"; }

	if ($conState == 5) {
		$xyText = "$char->{pos_to}{x}, $char->{pos_to}{y}";

		if ($AI) {
			if (@ai_seq) {
				my @seqs = @ai_seq;
				foreach (@seqs) {
					s/^route_//;
					s/_/ /g;
					s/([a-z])([A-Z])/$1 $2/g;
					$_ = lc $_;
				}
				substr($seqs[0], 0, 1) = uc substr($seqs[0], 0, 1);
				$aiText = join(', ', @seqs);
			} else {
				$aiText = "";
			}
		} else {
			$aiText = "Paused";
		}
	}

	# Only set status bar text if it has changed
	my $i = 0;
	my $setStatus = sub {
		if (defined $_[1] && $self->{$_[0]} ne $_[1]) {
			$self->{$_[0]} = $_[1];
			$self->{statusbar}->SetStatusText($_[1], $i);
		}
		$i++;
	};

	$setStatus->('statText', $statText);
	$setStatus->('clickText', $clickText);
	$setStatus->('xyText', $xyText);
	$setStatus->('aiText', $aiText);
}

sub updateMapViewer {
	my $self = shift;
	my $map = $self->{mapViewer};
	return unless ($map && $field && $char);

	my $myPos;
	$myPos = calcPosition($char);

	$map->set($field->name(), $myPos->{x}, $myPos->{y}, $field);
	my $i = AI::findAction("route");
	my $args;
	if (defined $i && ($args = AI::args($i)) && $args->{dest} && $args->{dest}{pos}) {
		$map->setDest($args->{dest}{pos}{x}, $args->{dest}{pos}{y});
	} else {
		$map->setDest;
	}

	my @players = values %players;
	$map->setPlayers(\@players);
	my @monsters = values %monsters;
	$map->setMonsters(\@monsters);
	my @npcs = values %npcs;
	$map->setNPCs(\@npcs);

	$map->update;
	$self->{mapViewTimeout}{time} = time;
}

sub updateItemList {
	my $self = shift;
###
		my ($endTime_EXP, $w_sec, $bExpPerHour, $jExpPerHour, $EstB_sec, $percentB, $percentJ, $zennyMade, $zennyPerHour, $EstJ_sec, $percentJhr, $percentBhr);
		$endTime_EXP = time;
		$w_sec = int($endTime_EXP - $startTime_EXP);
	if ($char) {
		if ($w_sec > 0) {
			$zennyMade = $char->{zenny} - $startingZenny;
			$bExpPerHour = int($totalBaseExp / $w_sec * 3600);
			$jExpPerHour = int($totalJobExp / $w_sec * 3600);
			$zennyPerHour = int($zennyMade / $w_sec * 3600);
			if ($char->{exp_max} && $bExpPerHour){
				$percentB = "(".sprintf("%.2f",$totalBaseExp * 100 / $char->{exp_max})."%)";
				$percentBhr = "(".sprintf("%.2f",$bExpPerHour * 100 / $char->{exp_max})."%)";
				$EstB_sec = int(($char->{exp_max} - $char->{exp})/($bExpPerHour/3600));
			}
			if ($char->{exp_job_max} && $jExpPerHour){
				$percentJ = "(".sprintf("%.2f",$totalJobExp * 100 / $char->{exp_job_max})."%)";
				$percentJhr = "(".sprintf("%.2f",$jExpPerHour * 100 / $char->{exp_job_max})."%)";
				$EstJ_sec = int(($char->{'exp_job_max'} - $char->{exp_job})/($jExpPerHour/3600));
			}
		}
		$char->{deathCount} = 0 if (!defined $char->{deathCount});
	}

###
	my $statuses = '';
	if ($char) {
		if (defined $char->{statuses} && %{$char->{statuses}}) {
			$statuses = join(", ", keys %{$char->{statuses}});
		}
	}

	my $msg1;
	my $msg2;

	if (!$AI) {
		$msg1 = "-=�����=-";
		$self->{AutoPause}->SetLabel("����");
	} elsif ($AI == 1) {
		$msg1 = "-=������=-";
		$self->{AutoPause}->SetLabel("����");
	} elsif ($AI == 2) {
		$msg1 = "";
		$self->{AutoPause}->SetLabel("�����");
	}
	if ($char) {
		if ($char->{sitting})	{$msg2 = "-=�����=-"; $self->{SitStand}->SetLabel("������");
		} else {$msg2 = ""; $self->{SitStand}->SetLabel("�����");};
	}
	my $msg = "$msg1 $msg2 $statuses";

###

	if ($conState == 5) {
		$self->{hpBar}->SetValue($char->{hp} / $char->{hp_max} * 100) if ($char->{hp_max});
		$self->{hpBar0}->SetLabel($char->{'hp'}."/".$char->{'hp_max'}." (".int($char->{'hp'}/$char->{'hp_max'} * 100)."%)");
		$self->{spBar}->SetValue($char->{sp} / $char->{sp_max} * 100) if ($char->{sp_max});
		$self->{spBar0}->SetLabel($char->{'sp'}."/".$char->{'sp_max'}." (".int($char->{'sp'}/$char->{'sp_max'} * 100)."%)");
		$self->{expBar}->SetValue($char->{exp} / $char->{exp_max} * 100) if ($char->{exp_max});
		$self->{jobExpBar}->SetValue($char->{exp_job} / $char->{exp_job_max} * 100) if ($char->{exp_job_max});
		$self->{zenys}->SetLabel(formatNumber($char->{'zenny'}));
		$self->{stat}->SetLabel($msg);
		$self->{weightBar}->SetLabel($char->{'weight'}."/".$char->{'weight_max'} .	" (" . sprintf("%.1f", $char->{'weight'}/$char->{'weight_max'} * 100). "%)");
		$self->{lvl}->SetLabel(timeConvert($EstB_sec));
		$self->{job}->SetLabel(timeConvert($EstJ_sec));
		$self->{zen}->SetLabel(formatNumber($zennyPerHour));
	}
}


##################
## Callbacks
##################


sub onInputEnter {
	my $self = shift;
	my $text = shift;
	my $command;

	my $n = $self->{inputType}->GetSelection;
	if ($n == 0 || $text =~ /^\/(.*)/) {
		my $command = ($n == 0) ? $text : $1;
		$self->{console}->add("input", "$command\n");
		$self->{inputBox}->Remove(0, -1);
		$self->{input} = $command;
		return;
	}

	if ($conState != 5) {
		$self->{console}->add("error", "You're not logged in.\n");
		return;
	}

	if ($self->{targetBox}->GetValue ne "") {
		sendMessage($messageSender, "pm", $text, $self->{targetBox}->GetValue);
	} elsif ($n == 1) { # Public chat
		sendMessage($messageSender, "c", $text);
	} elsif ($n == 2) { # Party chat
		sendMessage($messageSender, "p", $text);
	} else { # Guild chat
		sendMessage($messageSender, "g", $text);
	}
}

sub onTalkEnter {
	my $self = shift;
	my $text = shift;
	my $command;

	my $n = 0;

		my $command = ($n == 0) ? $text : $1;
		$self->{console}->SetDefaultStyle($self->{console}{inputStyle});
		$self->{console}->AppendText("talk num $command\n");
		$self->{console}->SetDefaultStyle($self->{console}{defaultStyle});
		$self->{inputBox}->Remove(0, -1);
		$self->{input} = "talk num $command";
		return;
}

sub onMenuOpen {
	my $self = shift;
	$self->{mPause}->Enable($AI);
	$self->{mManual}->Enable($AI != 1);
	$self->{mResume}->Enable($AI != 2);
#Party menu
	$self->{iParty}->Enable($config{partyAuto} !=0);
	$self->{aParty}->Enable($config{partyAuto} !=2);
	$self->{dParty}->Enable($config{partyAuto} !=1);
	$self->{shareParty1}->Enable($char->{'party'}{'share'} !=1);
	$self->{shareParty0}->Enable($char->{'party'}{'share'} !=0);
#Deal menu
	$self->{iDeal}->Enable($config{dealAuto} !=0);
	$self->{aDeal}->Enable($config{dealAuto} !=2);
	$self->{dDeal}->Enable($config{dealAuto} !=1);

	$self->{infoBarToggle}->Check($self->{infoPanel}->IsShown);
	$self->{chatLogToggle}->Check(defined $self->{notebook}->hasPage('Chat Log') ? 1 : 0);
	$self->{InvLogToggle}->Check(defined $self->{notebook}->hasPage('Inventory') ? 1 : 0);
}

sub onLoadFiles {
	my ($self, $hook, $param) = @_;
	if ($hook eq 'loadfiles') {
		$self->{loadingFiles}{percent} = $param->{current} / scalar(@{$param->{files}});
	} else {
		delete $self->{loadingFiles};
	}
}

sub onEnableAI {
	if ($AI ne 2) {
	$AI = 2;
	}else {
		Commands::run("move stop");
		$AI = 0;
		}
}

sub onManualAI {
	$AI = 1;
}

sub onDisableAI {
	Commands::run("move stop");
	$AI = 0;
}

sub onCopyLastOutput {
	my ($self) = @_;
	$self->{console}->copyLastLines(100);
}

sub onMinimizeToTray {
	my $self = shift;
	my $tray = new Wx::TaskBarIcon;
	my $title = ($char) ? "$char->{name} - $Settings::NAME" : "$Settings::NAME";
	$tray->SetIcon(Wx::GetWxPerlIcon, $title);
	EVT_TASKBAR_LEFT_DOWN($tray, sub {
		$tray->RemoveIcon;
		undef $tray;
		$self->{frame}->Show(1);
	});
	$self->{frame}->Show(0);
}

sub onClose {
	my ($self, $event) = @_;
	quit();
	if ($event->CanVeto) {
		$self->Show(0);
	}
}

sub onFontChange {
	my $self = shift;
	$self->{console}->selectFont($self->{frame});
}

sub onClearConsole {
	my $self = shift;
	$self->{console}->Remove(0, -1);
}

sub onAdvancedConfig {
	my $self = shift;
	if ($self->{notebook}->hasPage('Advanced Configuration')) {
		$self->{notebook}->switchPage('Advanced Configuration');
		return;
	}

	my $page = $self->{notebook}->newPage(1, 'Advanced Configuration');
	my $panel = new Wx::Panel($page, -1);

	my $vsizer = new Wx::BoxSizer(wxVERTICAL);
	$panel->SetSizer($vsizer);

	require Interface::Wx::ConfigEditor;
	my $cfg = new Interface::Wx::ConfigEditor($panel, -1);
	$cfg->setConfig(\%config);
	$cfg->addCategory('lockMap', 'Grid', ['lockMap', 'lockMap_x', 'lockMap_y', 'lockMap_randX', 'lockMap_randY']);
	$cfg->addCategory('Attacks', 'Grid', ['attackAuto', 'attackDistance', 'attackMaxDistance']);
	$cfg->addCategory('All', 'Grid');
	$cfg->onChange(sub {
		my ($key, $value) = @_;
		configModify($key, $value) if ($value ne $config{$key});
	});
	$vsizer->Add($cfg, 1, wxGROW | wxALL, 8);


	my $sizer = new Wx::BoxSizer(wxHORIZONTAL);
	$vsizer->Add($sizer, 0, wxGROW | wxLEFT | wxRIGHT | wxBOTTOM, 8);

	my $revert = new Wx::Button($panel, 46, '&Revert');
	$revert->SetToolTip('Revert settings to before you opened the selected category');
	$sizer->Add($revert, 0);
	EVT_BUTTON($revert, 46, sub {
		$cfg->revert;
	});
	$revert->Enable(0);
	$cfg->onRevertEnable(sub {
		$revert->Enable($_[0]);
	});

	my $pad = new Wx::Window($panel, -1);
	$sizer->Add($pad, 1);

	my $close = new Wx::Button($panel, 47, '&Close');
	$close->SetToolTip('Close this panel/dialog');
	$close->SetDefault;
	$sizer->Add($close, 0);
	EVT_BUTTON($close, 47, sub {
		$self->{notebook}->closePage('Advanced Configuration');
	});

	$page->set($panel);
}

sub onMapToggle {
	my $self = shift;
	$self->{mapDock}->attach;
}

sub onInfoBarToggle {
	my $self = shift;
	$self->{vsizer}->Show($self->{infoPanel}, $self->{infoBarToggle}->IsChecked);
	$self->{vsizer}->Show($self->{infoPanel2}, $self->{infoBarToggle}->IsChecked);
	$self->{vsizer}->Show($self->{infoPanel3}, $self->{infoBarToggle}->IsChecked);
	$self->{vsizer}->Show($self->{infoPanel5}, $self->{infoBarToggle}->IsChecked);
	$self->{frame}->Layout;
}

sub onChatLogToggle {
	my $self = shift;
	if (!$self->{chatLogToggle}->IsChecked) {
		$self->{notebook}->closePage('Chat Log');

	} elsif (!$self->{notebook}->hasPage('Chat Log')) {
		my $page = $self->{notebook}->newPage(1, 'Chat Log', 0);
		my $chatLog = $self->{chatLog} = new Interface::Wx::LogView($page);
		$page->set($chatLog);
		$chatLog->addColor("selfchat", 0, 148, 0);
		$chatLog->addColor("pm", 142, 120, 0);
		$chatLog->addColor("p", 164, 0, 143);
		$chatLog->addColor("g", 0, 177, 108);
		$chatLog->addColor("warning", 214, 93, 0);
		$page->set($chatLog);

	} else {
		$self->{notebook}->switchPage('Chat Log');
	}
}

sub onInvLogToggle {
	my $self = shift;
	if (!$self->{InvLogToggle}->IsChecked) {
		$self->{notebook}->closePage('Inventory');

	} elsif (!$self->{notebook}->hasPage('Inventory')) {
		my $page = $self->{notebook}->newPage(1, 'Inventory', 0);
		my $InvLog = $self->{InvLog} = new Interface::Wx::Inventory($page);
		$page->set($InvLog);
	} else {
		$self->{notebook}->switchPage('Inventory');
	}
}

sub onWMZ {
	my $self = shift;
	launchURL('http://clickcashmoney.com/in.htm?wm=106310');
}

sub onEgold {
	my $self = shift;
	launchURL('http://4059998.e-gold.com/');
}

sub onManual {
	my $self = shift;
	launchURL('http://manual.rofan.ru/');
}

sub onManual2 {
	my $self = shift;
	launchURL('http://openkore.sourceforge.net/manual/');
}

sub onForum {
	my $self = shift;
	launchURL('http://rofan.ru/index.php');
}

sub onTopic {
	my $self = shift;
	launchURL('http://rofan.ru/viewtopic.php?t=247');
}

sub onICQ {
	my $self = shift;
	launchURL('http://www.icq.com/people/about_me.php?uin=266048166');
}

sub onRouteStop {
	my $self = shift;
	Commands::run("conf route_randomWalk 0");
	Commands::run("move stop");
}

sub onBw_St_Sl_By {
	my $self = shift;
	Commands::run("is Butterfly Wing");
	Commands::run("autostorage");
	Commands::run("autosell");
	Commands::run("autobuy");
}

sub onChar0 {
	my $self = shift;
	Commands::run("conf char 0");
	Commands::run("relog");
}

sub onChar1 {
	my $self = shift;
	Commands::run("conf char 1");
	Commands::run("relog");
}

sub onChar2 {
	my $self = shift;
	Commands::run("conf char 2");
	Commands::run("relog");
}

sub onChar3 {
	my $self = shift;
	Commands::run("conf char 3");
	Commands::run("relog");
}

sub onItemListActivate {
	my ($self, $actor) = @_;

	if ($actor->isa('Actor::Player')) {
		Commands::run("lookp " . $actor->{binID});
		Commands::run("pl " . $actor->{binID});

	} elsif ($actor->isa('Actor::Monster')) {
		main::attack($actor->{ID});

	} elsif ($actor->isa('Actor::Item')) {
		$self->{console}->add("message", "Taking item " . $actor->nameIdx . "\n", "info");
		main::take($actor->{ID});

	} elsif ($actor->isa('Actor::NPC')) {
		Commands::run("nl " . $actor->{binID});
	}

	$self->{inputBox}->SetFocus;
}

sub onItemListRightClick {
	my ($self, $actor) = @_;

	if ($actor->isa('Actor::NPC')) {
		Commands::run("talk " . $actor->{binID}); 
	$self->{infoPanel7}->Show(1);
	$self->{frame}->Layout;
	}

	$self->{inputBox}->SetFocus;
}

sub onTargetBoxKeyDown {
	my $self = shift;
	my $event = shift;

	if ($event->GetKeyCode == WXK_TAB && !$event->ShiftDown) {
		$self->{inputBox}->SetFocus;

	} else {
		$event->Skip;
	}
}

sub onInitialized {
	my ($self) = @_;
	$self->{itemList}->init($npcsList, new Wx::Colour(103, 0, 162),
			$playersList, undef,
			$monstersList, new Wx::Colour(200, 0, 0),
			$itemsList, new Wx::Colour(0, 0, 200));
	my $dialog = new Interface::Wx::Start($self->{frame});
	$dialog->Show(1);

}

sub onAddPrivMsgUser {
	my $self = shift;
	my $param = $_[1];
	$self->{targetBox}->Append($param->{user});
}

sub onChatAdd {
	my ($self, $hook, $params) = @_;

	return if (!$self->{notebook}->hasPage('Chat Log'));
	if ($hook eq "ChatQueue::add" && $params->{type} ne "pm") {
		my $msg = '';
		if ($params->{type} ne "c") {
			$msg = "[$params->{type}] ";
		}
		$msg .= "$params->{user} : $params->{msg}\n";
		$self->{chatLog}->add($msg, $params->{type});

	} elsif ($hook eq "packet_selfChat") {
		# only display this message if it's a real self-chat
		$self->{chatLog}->add("$params->{user} : $params->{msg}\n", "selfchat") if ($params->{user});
	} elsif ($hook eq "packet_privMsg") {
		$self->{chatLog}->add("(From: $params->{privMsgUser}) : $params->{privMsg}\n", "pm");
	} elsif ($hook eq "packet_sentPM") {
		$self->{chatLog}->add("(To: $params->{to}) : $params->{msg}\n", "pm");
	}
}

sub onMapMouseMove {
	# Mouse moved over the map viewer control
	my ($self, undef, $args) = @_;
	my ($x, $y) = @{$args};
	my $walkable;

	$walkable = $field->isWalkable($x, $y);
	if ($x >= 0 && $y >= 0 && $walkable) {
		$self->{mouseMapText} = "Mouse over: $x, $y";
	} else {
		delete $self->{mouseMapText};
	}
	$self->{statusbar}->SetStatusText($self->{mouseMapText}, 0);
}

sub onMapClick {
	# Clicked on map viewer control
	my ($self, undef, $args) = @_;
	my ($x, $y) = @{$args};
	my $checkPortal = 0;
	delete $self->{mouseMapText};
	if ($self->{mapViewer} && $self->{mapViewer}->{portals}
		&& $self->{mapViewer}->{portals}->{$field->name()}
		&& @{$self->{mapViewer}->{portals}->{$field->name()}}){

		foreach my $portal (@{$self->{mapViewer}->{portals}->{$field->name()}}){
			if (distance($portal,{x=>$x,y=>$y}) <= 5) {
				$x = $portal->{x};
				$y = $portal->{y};
				$self->writeOutput("message", "Moving to Portal $x, $y\n", "info");
				$checkPortal = 1;
				last;
			}
		}
	}

	$self->writeOutput("message", "Moving to $x, $y\n", "info") unless $checkPortal;
	AI::clear("mapRoute", "route", "move");
	main::ai_route($field->name(), $x, $y, attackOnRoute => 1);
	$self->{inputBox}->SetFocus;
}

sub onMap_MapChange {
	my (undef, undef, undef, $mapDock) = @_;
	$mapDock->title($field->name());
	$mapDock->Fit;
}

sub onEmotions {
	my $self = shift;
	my $dialog = new Interface::Wx::Emotion($self->{frame});
	$dialog->Show(1);
}

sub onStats {
	my $self = shift;
	my $dialog = new Interface::Wx::Stats($self->{frame});
	$dialog->Show(1);
}

1;