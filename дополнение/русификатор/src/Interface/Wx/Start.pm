package Interface::Wx::Start;

use strict;
use Wx ':everything';
use Wx::Event qw(EVT_BUTTON);
use base qw(Wx::Dialog);
use Globals;

use constant DEFAULT_WIDTH => 250;

sub new {
	my ($class, $parent) = @_;
	my $self = $class->SUPER::new($parent, -1, "������");
	$self->_buildGUI();
	return $self;
}

sub _buildGUI {
	my ($self) = @_;
	my ($sizer, $label, $buttonSizer);

	$sizer = new Wx::BoxSizer(wxVERTICAL);
#	$label = new Wx::StaticText($self, -1, "StatPoint:$char->{'points_free'}");
#	$sizer->Add($label, 0, wxALIGN_CENTER, 0);
#	$label = new Wx::StaticText($self, -1, "SkillPoint:$char->{points_skill}");
#	$sizer->Add($label, 0, wxALIGN_CENTER, 0);
#	$label = new Wx::StaticText($self, -1, "Aspd:$char->{'attack_speed'}");
#	$sizer->Add($label, 0, wxALIGN_CENTER, 0);

#STR
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::StaticText($self, -1, "WMZ: Z612657389459");
	$buttonSizer->Add($label, 0, wxLEFT, 0);
#AGI
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::StaticText($self, -1, "WMR: R360744344389");
	$buttonSizer->Add($label, 0, wxALL, 0);
#VIT
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::StaticText($self, -1, "ICQ 266048166");
	$buttonSizer->Add($label, 0, wxALL, 0);

#INT
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::Button($self, 13, '���� �� ������))', wxDefaultPosition, [120, 17]);
	$buttonSizer->Add($label, 0);
	EVT_BUTTON($self, 13,  sub {$self->Destroy;});

	$self->SetSizerAndFit($sizer);
}

1;
