#########################################################################
#  $Id: Stats.pm
#  ICQ266048166 Click Wx-Interface for OpenKore 1.9.4 and hight
#  To change only from sanction Click
#########################################################################
package Interface::Wx::Stats;

use strict;
use Wx ':everything';
use Wx::Event qw(EVT_BUTTON);
use base qw(Wx::Dialog);
use Globals;

use constant DEFAULT_WIDTH => 250;

sub new {
	my ($class, $parent) = @_;
	my $self = $class->SUPER::new($parent, -1, "�����");
	$self->_buildGUI();
	return $self;
}

sub _buildGUI {
	my ($self) = @_;
	my ($sizer, $label, $buttonSizer);

	$sizer = new Wx::BoxSizer(wxVERTICAL);
#	$label = new Wx::StaticText($self, -1, "StatPoint:$char->{'points_free'}");
#	$sizer->Add($label, 0, wxALIGN_CENTER, 0);
#	$label = new Wx::StaticText($self, -1, "SkillPoint:$char->{points_skill}");
#	$sizer->Add($label, 0, wxALIGN_CENTER, 0);
#	$label = new Wx::StaticText($self, -1, "Aspd:$char->{'attack_speed'}");
#	$sizer->Add($label, 0, wxALIGN_CENTER, 0);

#STR
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::Button($self, 10, 'str +', wxDefaultPosition, [40, 17]);
	$buttonSizer->Add($label, 0, wxLEFT, 0);
	EVT_BUTTON($self, 10,  sub {Commands::run("stat_add str"); $self->Destroy;});

	$label = new Wx::StaticText($self, -1, "  $char->{'str'}+$char->{'str_bonus'} #$char->{'points_str'}   Atk: $char->{'attack'}+$char->{'attack_bonus'} Def: $char->{'def'}+$char->{'def_bonus'}");
	$buttonSizer->Add($label, 0, wxLEFT, 0);
#AGI
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::Button($self, 11, 'agi +', wxDefaultPosition, [40, 17]);
	$buttonSizer->Add($label, 0);
	EVT_BUTTON($self, 11,  sub {Commands::run("stat_add agi"); $self->Destroy;});

	$label = new Wx::StaticText($self, -1, "  $char->{'agi'}+$char->{'agi_bonus'} #$char->{'points_agi'}   Matk: $char->{'attack_magic_min'}~$char->{'attack_magic_max'} Mdef: $char->{'def_magic'}+$char->{'def_magic_bonus'}");
	$buttonSizer->Add($label, 0, wxALL, 0);
#VIT
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::Button($self, 12, 'vit +', wxDefaultPosition, [40, 17]);
	$buttonSizer->Add($label, 0);
	EVT_BUTTON($self, 12,  sub {Commands::run("stat_add vit"); $self->Destroy;});

	$label = new Wx::StaticText($self, -1, "  $char->{'vit'}+$char->{'vit_bonus'} #$char->{'points_vit'}   Hit:  $char->{'hit'}   Flee: $char->{'flee'}+$char->{'flee_bonus'}");
	$buttonSizer->Add($label, 0, wxALL, 0);

#INT
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::Button($self, 13, 'int +', wxDefaultPosition, [40, 17]);
	$buttonSizer->Add($label, 0);
	EVT_BUTTON($self, 13,  sub {Commands::run("stat_add int"); $self->Destroy;});

	$label = new Wx::StaticText($self, -1, "  $char->{'int'}+$char->{'int_bonus'} #$char->{'points_int'}   Critical: $char->{'critical'} Aspd: $char->{'attack_speed'}");
	$buttonSizer->Add($label, 0, wxALL, 0);

#DEX
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::Button($self, 14, 'dex +', wxDefaultPosition, [40, 17]);
	$buttonSizer->Add($label, 0);
	EVT_BUTTON($self, 14,  sub {Commands::run("stat_add dex"); $self->Destroy;});

	$label = new Wx::StaticText($self, -1, "  $char->{'dex'}+$char->{'dex_bonus'} #$char->{'points_dex'}   StatPoint: $char->{'points_free'}");
	$buttonSizer->Add($label, 0, wxALL, 0);

#LUK
	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxLEFT, 0);

	$label = new Wx::Button($self, 15, 'luk +', wxDefaultPosition, [40, 17]);
	$buttonSizer->Add($label, 0);
	EVT_BUTTON($self, 15,  sub {Commands::run("stat_add luk"); $self->Destroy;});

	$label = new Wx::StaticText($self, -1, "  $char->{'luk'}+$char->{'luk_bonus'} #$char->{'points_luk'}   SkillPoint: $char->{points_skill}");
	$buttonSizer->Add($label, 0, wxALL, 0);

	$label = new Wx::StaticText($self, -1, "Walk speed: $char->{walk_speed} secs per block");
	$sizer->Add($label, 0, wxALIGN_CENTER, 0);

	$self->SetSizerAndFit($sizer);
}

1;
