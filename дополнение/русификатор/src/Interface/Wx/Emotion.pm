#########################################################################
#  $Id: Emotion.pm
#  ICQ266048166 Click Wx-Interface for OpenKore 1.9.4 and hight
#  To change only from sanction Click
#########################################################################
package Interface::Wx::Emotion;

use strict;
use Wx ':everything';
use Wx::Event qw(EVT_BUTTON);
use base qw(Wx::Dialog);

use constant DEFAULT_WIDTH => 250;

sub new {
	my ($class, $parent) = @_;
	my $self = $class->SUPER::new($parent, -1, "������");
	$self->_buildGUI();
	return $self;
}

sub _buildGUI {
	my ($self) = @_;
	my ($sizer, $label, $buttonSizer, $check);

	$sizer = new Wx::BoxSizer(wxVERTICAL);

	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM, 0);

		my $handler = new Wx::GIFHandler();
		Wx::Image::AddHandler($handler);

		my $filename = "bitmaps/emotion/!.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 101, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 101,	sub { Commands::run("e !"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Question.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 102, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 102,	sub { Commands::run("e ?"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Whistling.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 103, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 103,	sub { Commands::run("e ho"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Heart.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 104, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 104,	sub { Commands::run("e lv"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Love.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 105, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 105,	sub { Commands::run("e lv2"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/2Hearts.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 106, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 106,	sub { Commands::run("e awsm"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Kiss.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 107, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 107,	sub { Commands::run("e kis"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Kiss2.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 108, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 108,	sub { Commands::run("e kis2"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM, 0);

		my $filename = "bitmaps/emotion/Sweat.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 109, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 109,	sub { Commands::run("e swt"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Sweat2.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 110, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 110,	sub { Commands::run("e swt2"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Sob.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 111, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 111,	sub { Commands::run("e sob"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/!@#.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 112, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 112,	sub { Commands::run("e an"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Grumble.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 113, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 113,	sub { Commands::run("e ag"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Brovi.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 114, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 114,	sub { Commands::run("e crwd"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Down.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 115, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 115,	sub { Commands::run("e desp"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Dumb.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 116, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 116,	sub { Commands::run("e dum"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM, 0);

		my $filename = "bitmaps/emotion/Idea.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 117, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 117,	sub { Commands::run("e ic"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Zeny.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 118, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 118,	sub { Commands::run("e zeny"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/....gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 119, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 119,	sub { Commands::run("e ..."); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Thanks.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 120, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 120,	sub { Commands::run("e thx"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Sorry.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 121, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 121,	sub { Commands::run("e sry"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Wah.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 122, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 122,	sub { Commands::run("e wah"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Heh.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 123, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 123,	sub { Commands::run("e heh"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Hmm.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 124, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 124,	sub { Commands::run("e hmm"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM, 0);

		my $filename = "bitmaps/emotion/No.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 125, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 125,	sub { Commands::run("e ??"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Yes.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 126, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 126,	sub { Commands::run("e ok"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Nice_One.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 127, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 127,	sub { Commands::run("e no1"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Omg.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 128, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 128,	sub { Commands::run("e omg"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/O.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 129, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 129,	sub { Commands::run("e oh"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/X.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 130, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 130,	sub { Commands::run("e x"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Help.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 131, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 131,	sub { Commands::run("e hp"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Go.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 132, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 132,	sub { Commands::run("e go"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM, 0);

		my $filename = "bitmaps/emotion/Good_Game.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 133, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 133,	sub { Commands::run("e gg"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Pfft.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 134, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 134,	sub { Commands::run("e pif"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/'o'.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 135, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 135,	sub { Commands::run("e sigh"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Bleks.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 136, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 136,	sub { Commands::run("e meh"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Na-ah.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 137, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 137,	sub { Commands::run("e shy"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Pat_Pat.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 138, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 138,	sub { Commands::run("e pat"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Yawn.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 139, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 139,	sub { Commands::run("e yawn"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Drool.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 140, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 140,	sub { Commands::run("e rice"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM, 0);

		my $filename = "bitmaps/emotion/SP.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 141, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 141,	sub { Commands::run("e mp"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/HP.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 142, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 142,	sub { Commands::run("e e12"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Horny.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 143, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 143,	sub { Commands::run("e slur"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Come_Here.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 144, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 144,	sub { Commands::run("e com"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Congratulations.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 145, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 145,	sub { Commands::run("e grat"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/FlashEyes.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 146, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 146,	sub { Commands::run("e fsh"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/SpinEyes.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 147, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 147,	sub { Commands::run("e spin"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Angry.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 148, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 148,	sub { Commands::run("e bzz"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM, 0);

		my $filename = "bitmaps/emotion/Ruka1.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 149, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 149,	sub { Commands::run("e fst"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Ruka2.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 150, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 150,	sub { Commands::run("e pea"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Flag1.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 151, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 151,	sub { Commands::run("e flg"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Flag2.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 152, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 152,	sub { Commands::run("e flg2"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Flag3.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 153, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 153,	sub { Commands::run("e flg3"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Flag4.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 154, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 154,	sub { Commands::run("e flg4"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Flag5.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 155, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 155,	sub { Commands::run("e flg5"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Flag6.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 156, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 156,	sub { Commands::run("e flg6"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});


	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM, 0);

		my $filename = "bitmaps/emotion/Ruka3.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 157, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 157,	sub { Commands::run("e wav"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Dice1.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 158, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 158,	sub { Commands::run("e dice1"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Dice2.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 159, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 159,	sub { Commands::run("e dice2"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Dice3.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 160, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 160,	sub { Commands::run("e dice3"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Dice4.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 161, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 161,	sub { Commands::run("e dice4"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Dice5.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 162, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 162,	sub { Commands::run("e dice5"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $filename = "bitmaps/emotion/Dice6.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 163, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 163,	sub { Commands::run("e dice6"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

		my $dice =  int(rand(6))+1;

		my $filename = "bitmaps/emotion/Dice_Random.gif";
		my $image = Wx::Image->newNameType($filename, wxBITMAP_TYPE_ANY);
                	my $bitmap = Wx::Bitmap->new($image);
	$label = new Wx::BitmapButton($self, 164, $bitmap, wxDefaultPosition, [24, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 164,	sub { Commands::run("e dice$dice"); if ($self->{check}->GetValue() eq "1") {$self->Destroy;}});

	$buttonSizer = new Wx::BoxSizer(wxHORIZONTAL);
	$sizer->Add($buttonSizer, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM, 0);

	$label = $self->{check} = new Wx::CheckBox($self, 165, "��������� ����", wxDefaultPosition, [100, 24],
		wxGA_HORIZONTAL | wxGA_SMOOTH);
		$buttonSizer->Add($label, 0);
		EVT_BUTTON($label, 165,	$self->{check}->SetValue(1));

	$self->SetSizerAndFit($sizer);
}

1;
